import massive.munit.TestSuite;

import conanhaxe.test.unit.ProjectLocatorTest;
import conanhaxe.test.features.InstallDependenciesFeatureTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(conanhaxe.test.unit.ProjectLocatorTest);
		add(conanhaxe.test.features.InstallDependenciesFeatureTest);
	}
}
