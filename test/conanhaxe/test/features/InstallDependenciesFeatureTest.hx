package conanhaxe.test.features;

import massive.munit.Assert;
import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;
import org.hamcrest.Matchers.*;

import conanhaxe.test.matchers.CaptureMatcher;

import conanhaxe.Installer;
import conanhaxe.ConanBridge;
import conanhaxe.HxcppBuild;
import conanhaxe.model.*;

class InstallDependenciesFeatureTest implements hxgiven.Scenario{

  @steps
  var steps:InstallDependenciesSteps;

  @Test
  @scenario
  public function install_conan_dependencies_of_current_project() {
    given().i_am_running_at_a_project_with_a_conanfile();
    when().installing_dependencies_for_that_project();
    then()
      .the_conanfile_of_current_project_should_be_located_and_installed();
      and().a_file_should_be_generated_to_integrate_the_conan_dependencies_with_hxcpp();
      and().that_file_should_be_included_with_the_project();
  }

  @Test
  @Ignore('Not implemented yet')
  @scenario
  public function install_conan_dependencies_of_required_libraries() {
    throw 'Not implemented yet';
  }
}


class InstallDependenciesSteps extends hxgiven.Steps<InstallDependenciesSteps> {
  var installer:Installer;

  var currentProject:Project;
  var fakeProjects: Array<Project>;
  var classpaths: Array<String>;

  var mockConan:ConanBridge;
  var mockLocator:ProjectLocator;
  var mockBuild:HxcppBuild;

  var captureInstallOptions:CaptureMatcher<ConanInstallOptions>;

  public function new() {
    super();

    fakeProjects = [];
    classpaths = [];

    mockLocator = mock(ProjectLocator);
    mockConan = mock(ConanBridge);
    mockBuild = mock(HxcppBuild);

    installer = new Installer();
    installer.locator = mockLocator;
    installer.conan = mockConan;
    installer.hxcppBuild = mockBuild;

    captureInstallOptions = new CaptureMatcher<ConanInstallOptions>();

    // Mockatoo.when(mockLocator.locate(any)).thenReturn([]);
  }

  @step
  public function i_am_running_at_a_project_with_a_conanfile() {
    currentProject = new Project({
      root: '/path/to/my/project',
      conanfile: 'conanfile.py'
    });

    Mockatoo.when(mockLocator.locate(arrayIsEqualTo(['.']))).thenReturn([currentProject]);
  }

  @step
  public function installing_dependencies_for_that_project() {
    Assert.isNotNull(classpaths);

    installer.install(classpaths);
  }

  @step
  public function the_conanfile_of_current_project_should_be_located_and_installed() {
    Mockatoo.verify(mockLocator.locate(arrayIsEqualTo(['.'])));

    Mockatoo.verify(mockConan.install(currentProject, captureInstallOptions.matcher()));
  }

  @step
  public function a_file_should_be_generated_to_integrate_the_conan_dependencies_with_hxcpp() {
    var installOptions = captureInstallOptions.captured;

    assertThat(installOptions, is(notNullValue()));
    assertThat(installOptions.useGenerator, is(true));
  }

  @step
  public function that_file_should_be_included_with_the_project() {
    Mockatoo.verify(mockBuild.include(currentProject, Constants.generatorFile));
  }

  /* ************************************************************************ */

  function arrayIsEqualTo<T>( array: Array<T> ) {
    return customMatcher(function ( arg: Dynamic ) {
      if( arg == null ) {
        return (array == null);
      }

      var capturedArray = (arg: Array<T>);

      if( capturedArray.length != array.length ) {
        return false;
      }

      for( i in 0...array.length ) {
        if( capturedArray[i] != array[i] ) {
          return false;
        }
      }


      return true;
    });
  }
}
