package conanhaxe.test.unit;

import haxe.io.Path;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;
import org.hamcrest.Matchers.*;

import conanhaxe.ProjectLocator;
import conanhaxe.Files;
import conanhaxe.model.Project;

class ProjectLocatorTest extends TestScenario{
  static var root = '/';
  static var PROJECT_DIR = [root, 'path', 'myproject'];
  static var PROJECT_SRC_DIR = PROJECT_DIR.concat(['src']);
  static var PROJECT_HAXELIB_JSON  = PROJECT_DIR.concat(['haxelib.json']);
  static var PROJECT_CONANFILE_PY  = PROJECT_DIR.concat(['conanfile.py']);
  static var PROJECT_CONANFILE_TXT = PROJECT_DIR.concat(['conanfile.txt']);

  @steps
  var steps:ProjectLocatorSteps;


  @Test
  @scenario
  public function locate_project_with_haxelib_file( ) {
    given().a_classpath_of([PROJECT_SRC_DIR]);
    given().there_is_a_file_at(PROJECT_HAXELIB_JSON);
    when().searching_for_projects();
    then().it_should_found_these_projects([
      new Project({
        root     : Path.join(PROJECT_DIR),
        isHaxelib: true
      })
    ]);
  }

  @Test
  @scenario
  public function locate_project_with_conanfile() {
    given().a_classpath_of([PROJECT_SRC_DIR]);
    given().there_is_a_conanfile_at(PROJECT_DIR);
    when().searching_for_projects();
    then().it_should_found_these_projects([
      new Project({
        root     : Path.join(PROJECT_DIR),
        conanfile: 'conanfile.py'
      })
    ]);
  }

  @Test
  @scenario
  public function locate_project_with_conanfile_txt() {
    given().a_classpath_of([PROJECT_SRC_DIR]);
    given().there_is_a_file_at(PROJECT_CONANFILE_TXT);
    when().searching_for_projects();
    then().it_should_found_these_projects([
      new Project({
        root     : Path.join(PROJECT_DIR),
        conanfile: 'conanfile.txt'
      })
    ]);
  }


  @Test
  @scenario
  public function locate_project_with_conanfile_py_and_conanfile_txt() {
    given().a_classpath_of([PROJECT_SRC_DIR]);
    given().there_is_a_file_at(PROJECT_CONANFILE_TXT);
    given().there_is_a_file_at(PROJECT_CONANFILE_PY);
    when().searching_for_projects();
    then().it_should_found_these_projects([
      new Project({
        root     : Path.join(PROJECT_DIR),
        conanfile: 'conanfile.py'
      })
    ]);
  }

  @Test
  @scenario
  public function locate_project_with_haxelib_and_conanfile( ) {
    given().a_classpath_of([PROJECT_SRC_DIR]);
    given().there_is_a_file_at(PROJECT_HAXELIB_JSON);
    given().there_is_a_file_at(PROJECT_CONANFILE_PY);
    when().searching_for_projects();
    then().it_should_found_these_projects([
      new Project({
        root     : Path.join(PROJECT_DIR),
        conanfile: 'conanfile.py',
        isHaxelib: true
      })
    ]);
  }

  @Test
  @scenario
  public function eliminate_duplicated_projects( ) {
    given().a_classpath_of([
      PROJECT_SRC_DIR,
      PROJECT_DIR.concat(['test'])
    ]);
    given().there_is_a_conanfile_at(PROJECT_DIR);
    when().searching_for_projects();
    then().it_should_found_these_projects([
      new Project({
        root     : Path.join(PROJECT_DIR),
        conanfile: 'conanfile.py'
      })
    ]);

  }
}


class ProjectLocatorSteps extends hxgiven.Steps<ProjectLocatorSteps> {

  var classpath:Array<String>;

  var locator:ProjectLocator;
  var mockFiles:Files;

  var locatedFiles:Array<String>;
  var locatedProjects:Array<Project>;

  public function new() {
    super();

    classpath = [];

    mockFiles = spy(Files);

    locator = new ProjectLocator();
    locator.files = mockFiles;
  }

  @step
  public function a_classpath_of( paths: Array<Array<String>> ) {
    for( path in paths ) {
      this.classpath.push(toPath(path));
    }
  }

  @step
  public function there_is_a_conanfile_at( path: Array<String> ) {
    given().there_is_a_file_at(path.concat(['conanfile.py']));
  }

  @step
  public function there_is_a_file_at( path: Array<String> ) {
    var pathStr = toPath(path);
    Mockatoo.when(mockFiles.exists(pathStr)).thenReturn(true);
  }

  @step
  public function searching_for_projects() {
    locatedProjects = locator.locate(this.classpath);
  }

  @step
  public function it_should_found_these_projects( projects: Array<Project> ) {
    assertThat(this.locatedProjects, is(equalTo(projects)));
  }

  function toPath( path: Array<String> ) : String {
    return Path.join(path);
  }
}
