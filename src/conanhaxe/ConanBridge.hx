package conanhaxe;

import conanhaxe.model.Project;

typedef ConanInstallOptions = {
  ?useGenerator:Bool
};

class ConanBridge
{
    public function new()
    {

    }

    public function install(p: Project, ?options: ConanInstallOptions) {

    }
}
