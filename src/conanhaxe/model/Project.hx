package conanhaxe.model;

typedef ProjectArgs = {
  ?root     : String,
  ?conanfile: String,
  ?isHaxelib: Bool
}

class Project{

  public var rootDir(default, default):String;
  public var conanfile(default, default):String;

  public function new(?args: ProjectArgs){
    args = if( args != null ) args else {};

    this.rootDir   = args.root;
    this.conanfile = args.conanfile;
  }

  public function equals( other: Project ) : Bool {
    return other != null
        && rootDir   == other.rootDir
        && conanfile == other.conanfile;
  }
}
