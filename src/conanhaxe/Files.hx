package conanhaxe;

import sys.FileSystem;

class Files
{
    public function new()
    {

    }

    public function exists( path: String ) : Bool {
      return FileSystem.exists(path);
    }

    public function absolutePath( path:String ) : String {
      return FileSystem.absolutePath(path);
    }
}
