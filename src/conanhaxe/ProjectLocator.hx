package conanhaxe;

import haxe.io.Path;

import conanhaxe.model.Project;

class ProjectLocator{

  public var files(default, default): Files;

  public function new()
  {
    this.files = new Files();
  }

  public function locate( pathsToSearch:Array<String> ) : Array<Project> {
    var located = new Map<String, Project>();

    for( p in pathsToSearch ) {
      locateProjectFrom(p, located);
    }

    return [for(proj in located) proj];
  }

  function locateProjectFrom(fromPath: String, located: Map<String, Project>) : Void {
    var p = findProject(fromPath);

    if( p != null && !located.exists(p.rootDir) ) {
      located.set(p.rootDir, p);
    }
  }

  function findProject( fromPath: String ) : Project {
    var pathStr = files.absolutePath(fromPath);
    var path = new Path(pathStr);

    while( path.file != null && path.file.length > 0 ) {
      var conanfile:String = findConanfile(pathStr);
      var hasHaxelib:Bool  = exists([pathStr, 'haxelib.json']);

      if( conanfile != null || hasHaxelib ) {
        return new Project({
          root     : pathStr,
          conanfile: conanfile,
          isHaxelib: hasHaxelib
        });
      }

      //updates path (up one level from the directory tree)
      pathStr = path.dir;
      path = new Path(pathStr);
    }

    return null;
  }

  function findConanfile( dirPath: String ) : String {
    for( file in ['conanfile.py', 'conanfile.txt'] ) {
      if( exists([dirPath, file]) ) {
        return file;
      }
    }

    return null;
  }

  function exists( pathArray: Array<String> ) : Bool {
    return files.exists(Path.join(pathArray));
  }
}
