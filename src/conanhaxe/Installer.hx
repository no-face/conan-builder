package conanhaxe;

import conanhaxe.model.Project;
import conanhaxe.model.Constants;

class Installer {

  public var locator(default, default): ProjectLocator;
  public var conan(default, default): ConanBridge;
  public var hxcppBuild(default, default): HxcppBuild;

  public function new(){
    locator = new ProjectLocator();
    conan = new ConanBridge();
    hxcppBuild = new HxcppBuild();
  }

  public function install(classpaths: Array<String>) : Void {
    var currentProject = locateCurrentProject();

    if( currentProject != null ) {
      conan.install(currentProject, { useGenerator: true });

      hxcppBuild.include(currentProject, Constants.generatorFile);
    }

  }

  function locateCurrentProject() : Null<Project> {
    var found = locator.locate(['.']);

    if(found != null && found.length > 0) {
      return found[0];
    }

    return null;
  }
}
